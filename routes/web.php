<?php

use Illuminate\Support\Facades\Route;
Route::get('/', function(){
    return view('login');
});
Route::get('/create', function(){
    return view('create');
});
Route::post('/login', 'AuthController@login')->name('login');
Route::post('/crear', 'NewController@nuevo')->name('crear');
Route::get('/logout', 'AuthController@logout')->name('logout');

//Route::get('/k-means', 'PagesController@kmeans')->name('algoritmo');


Route::group(['middleware' => ['web', 'custom_auth']], function () {
    Route::get('/analisis', 'PagesController@home')->name('home');
    Route::get('/puntos', 'CoordenadasController@puntos')->name('puntito');
    Route::get('/algoritmo', 'CoordenadasController@algoritmo')->name('kmeans');
});