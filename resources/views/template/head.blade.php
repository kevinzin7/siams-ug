<head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <!-- Meta, title, CSS, favicons, etc. -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" href="images/favicon.ico" type="image/ico" />

      <title>SIAMS | @yield('title')</title>

      <!-- Bootstrap -->
      <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
      <!-- NProgress -->
      <link href="{{ asset('css/nprogress.css') }}" rel="stylesheet">
      <!-- iCheck -->
      <link href="{{ asset('css/flat/green.css') }}" rel="stylesheet">

      <!-- bootstrap-progressbar -->
      <link href="{{ asset('css/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet">
      <!-- JQVMap -->
      <link href="{{ asset('css/jqvmap.min.css') }}" rel="stylesheet" />
      <!-- bootstrap-daterangepicker -->
      <link href="{{ asset('css/daterangepicker.css') }}" rel="stylesheet">

      <!-- Custom Theme Style -->
      <link href="{{ asset('css/custom.min.css') }}" rel="stylesheet">
      <link href="{{ asset('css/leaflet.css') }}" rel="stylesheet">
      <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">

      <script src="https://kit.fontawesome.com/dfea41f182.js" crossorigin="anonymous"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

      @yield('estilo')
      <link href="{{ asset('css/base.css') }}" rel="stylesheet">
      
  </head>